package pl.spyplacz.blackpowder;

import pl.spyplacz.blackpowder.entity.Weapon;
import pl.spyplacz.blackpowder.repository.WeaponRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
    @SpringBootTest
    public class WeaponRepoTest {

        @Autowired
        private WeaponRepository weaponRepository;

        @Test
        public void testFindById(){
            Optional<Weapon> weapon= weaponRepository.findById(1);
//            double caliberInches = weapon.get().getCaliber();

            Assert.assertEquals(1, weapon.get().getId());
        }
    }



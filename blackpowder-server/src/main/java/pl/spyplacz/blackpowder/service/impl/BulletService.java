package pl.spyplacz.blackpowder.service.impl;

import pl.spyplacz.blackpowder.converter.BulletConverter;
import pl.spyplacz.blackpowder.dto.BulletDto;
import pl.spyplacz.blackpowder.entity.Bullet;
import pl.spyplacz.blackpowder.repository.BulletRepository;
import pl.spyplacz.blackpowder.service.api.IBulletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BulletService implements IBulletService {

    @Autowired
    BulletRepository bulletRepository;

    @Override
    public List<BulletDto> getAllBullets() {
        return bulletRepository.findAll().stream().map(BulletConverter::toDto).collect(Collectors.toList());
    }

    @Override
    public Optional<BulletDto> getBulletById(int id) {
        return Optional.of(BulletConverter.toDto(bulletRepository.getOne(id)));
    }

    @Override
    public List<BulletDto> getBulletByInches(double inches) {
        return bulletRepository.getBulletByInches(inches).stream().map(BulletConverter::toDto).collect(Collectors.toList());
    }

    @Override
    public List<BulletDto> getBulletByInchesRange(double from, double to) {
        return bulletRepository.getBulletByInchesRange(from, to).stream().map(BulletConverter::toDto).collect(Collectors.toList());
    }

    @Override
    public List<BulletDto> getBulletByInchesAndType(double inches, String type) {
        return bulletRepository.getBulletByInchesAndType(inches, type).stream().map(BulletConverter::toDto).collect(Collectors.toList());
    }

    @Override
    public void save(BulletDto bulletDto) {
        bulletRepository.save(BulletConverter.toEntity(bulletDto));
    }

    @Override
    public void updateBullet(int id, BulletDto dto) {
        Bullet bullet = bulletRepository.getOne(id);
        bullet.setCaliber(dto.getCaliber());
        bullet.setType(dto.getType());
        bulletRepository.save(bullet);
    }

    @Override
    public void removeBullet(int id) {
        bulletRepository.deleteById(id);
    }

}

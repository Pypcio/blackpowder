package pl.spyplacz.blackpowder.service.api;

import pl.spyplacz.blackpowder.dto.WeaponDto;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
public interface IWeaponService {

    List<WeaponDto> getAllWeapons();

    List<WeaponDto> getWeaponsByCaliber(double from, double to);

    Optional<WeaponDto> getWeaponById(int id);

    void saveWeapon(WeaponDto weaponDto);

    void removeWeapon(int id);

    int getLastId();

    void updateWeapon(int id, WeaponDto weaponDto);
}

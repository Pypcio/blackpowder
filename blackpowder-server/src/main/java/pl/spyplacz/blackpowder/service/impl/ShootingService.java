package pl.spyplacz.blackpowder.service.impl;

import pl.spyplacz.blackpowder.converter.ShootingConverter;
import pl.spyplacz.blackpowder.dto.ShootingDto;
import pl.spyplacz.blackpowder.entity.Shooting;
import pl.spyplacz.blackpowder.repository.ShootingRepository;
import pl.spyplacz.blackpowder.service.api.IShootingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ShootingService implements IShootingService {

    @Autowired
    ShootingRepository shootingRepository;

    @Override
    public List<ShootingDto> getAllShooting() {
        return shootingRepository.findAll().stream().map(ShootingConverter::toDto).collect(Collectors.toList());
    }

    @Override
    public Optional<ShootingDto> getById(int id) {
        return Optional.ofNullable(ShootingConverter.toDto(shootingRepository.getOne(id)));
    }

    @Override
    public Optional<ShootingDto> getLast() {
        List<Shooting> lastShooting = shootingRepository.getLastShooting();
        return Optional.ofNullable(ShootingConverter.toDto(lastShooting.get(0)));
    }

    @Override
    public Optional<ShootingDto> getBest() {
        List<ShootingDto> lastShooting = getAllShooting();
        return lastShooting.stream().max(Comparator.comparing(ShootingDto::getMean));
    }

    @Override
    public void saveShooting(ShootingDto shootingDto) {
        shootingRepository.save(ShootingConverter.toEntity(shootingDto));
    }

    @Override
    public void removeById(int id) {
        shootingRepository.delete(shootingRepository.getOne(id));
    }

    @Override
    public void updateShooting(int id, ShootingDto shootingDto) {
        ShootingDto entity = ShootingConverter.toDto(shootingRepository.getOne(id));

        entity.setWeapon(shootingDto.getWeapon());
        entity.setBullet(shootingDto.getBullet());
        entity.setDate(shootingDto.getDate());
        entity.setDistance(shootingDto.getDistance());
        entity.setPowderGrains(shootingDto.getPowderGrains());
        entity.setZeroResult(shootingDto.getZeroResult());
        entity.setOneResult(shootingDto.getOneResult());
        entity.setTwoResult(shootingDto.getTwoResult());
        entity.setThreeResult(shootingDto.getThreeResult());
        entity.setFourResult(shootingDto.getFourResult());
        entity.setFiveResult(shootingDto.getFiveResult());
        entity.setSixResult(shootingDto.getSixResult());
        entity.setSevenResult(shootingDto.getSevenResult());
        entity.setEightResult(shootingDto.getEightResult());
        entity.setNineResult(shootingDto.getNineResult());
        entity.setTenResult(shootingDto.getTenResult());

        Shooting shooting = ShootingConverter.toEntity(shootingDto);
        shooting.setId(id);
        shootingRepository.saveAndFlush(shooting);
    }
}

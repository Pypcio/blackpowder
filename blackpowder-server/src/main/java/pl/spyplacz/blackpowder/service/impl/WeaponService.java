package pl.spyplacz.blackpowder.service.impl;

import pl.spyplacz.blackpowder.converter.WeaponConverter;
import pl.spyplacz.blackpowder.dto.WeaponDto;
import pl.spyplacz.blackpowder.repository.WeaponRepository;
import pl.spyplacz.blackpowder.service.api.IWeaponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class WeaponService implements IWeaponService {

    @Autowired
    WeaponRepository weaponRepository;

    @Override
    public List<WeaponDto> getAllWeapons() {
        return weaponRepository.findAll().stream().map(WeaponConverter::toDto).collect(Collectors.toList());
    }

    @Override
    public List<WeaponDto> getWeaponsByCaliber(double from, double to) {
        return weaponRepository.getWeaponByCaliber(from, to).stream().map(WeaponConverter::toDto).collect(Collectors.toList());
    }

    @Override
    public Optional<WeaponDto> getWeaponById(int id) {
        return Optional.ofNullable(WeaponConverter.toDto(weaponRepository.getOne(id)));
    }

    @Override
    public void saveWeapon(WeaponDto weaponDto) {
        weaponRepository.save(WeaponConverter.toEntity(weaponDto));
    }

    @Override
    public void removeWeapon(int id) {
        weaponRepository.deleteById(id);
    }

    @Override
    public int getLastId(){
        return weaponRepository.getWeaponSize();
    }

    @Override
    public void updateWeapon(int id, WeaponDto weaponDto) {
        WeaponDto entity = WeaponConverter.toDto(weaponRepository.getOne(id));
        entity.setCaliber(weaponDto.getCaliber());
        entity.setName(weaponDto.getName());
        entity.setShotAmount(weaponDto.getShotAmount());
        entity.setType(weaponDto.getType());
        weaponRepository.save(WeaponConverter.toEntity(entity));
    }
}

package pl.spyplacz.blackpowder.service.api;

import pl.spyplacz.blackpowder.dto.ShootingDto;

import java.util.List;
import java.util.Optional;

public interface IShootingService {

    List<ShootingDto> getAllShooting();

    Optional<ShootingDto> getById(int id);

    Optional<ShootingDto> getLast();

    Optional<ShootingDto> getBest();

    void saveShooting(ShootingDto shootingDto);

    void removeById(int id);

    void updateShooting(int id, ShootingDto shootingDto);
}

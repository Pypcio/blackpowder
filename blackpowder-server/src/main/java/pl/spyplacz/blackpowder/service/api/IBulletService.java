package pl.spyplacz.blackpowder.service.api;

import pl.spyplacz.blackpowder.dto.BulletDto;

import java.util.List;
import java.util.Optional;

public interface IBulletService {

    List<BulletDto> getAllBullets();

    Optional<BulletDto> getBulletById(int id);

    List<BulletDto> getBulletByInches(double inches);

    List<BulletDto> getBulletByInchesRange(double from, double to);

    List<BulletDto> getBulletByInchesAndType(double inches, String type);

    void save(BulletDto bulletDto);

    void updateBullet(int id, BulletDto dto);

    void removeBullet(int id);

}

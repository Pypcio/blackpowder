package pl.spyplacz.blackpowder.repository;

import pl.spyplacz.blackpowder.entity.Weapon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
@CrossOrigin(origins = "http://localhost:4200")
public interface WeaponRepository extends JpaRepository<Weapon, Integer> {

    @Query("select weapon from Weapon weapon where weapon.caliber >= (:from) and weapon.caliber <= (:to)")
    List<Weapon> getWeaponByCaliber(@Param("from") double from, @Param("to") double to);

    @Query("select count (weapon) from Weapon weapon")
    int getWeaponSize();
}

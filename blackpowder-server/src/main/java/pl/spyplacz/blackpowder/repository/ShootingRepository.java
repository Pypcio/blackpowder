package pl.spyplacz.blackpowder.repository;

import pl.spyplacz.blackpowder.entity.Shooting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
@CrossOrigin(origins = "http://localhost:4200")
public interface ShootingRepository extends JpaRepository<Shooting, Integer> {

    @Query("select shot from Shooting shot where 1 = 1 order by shot.date desc")
    List<Shooting> getLastShooting();
}

package pl.spyplacz.blackpowder.repository;

import pl.spyplacz.blackpowder.entity.Bullet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
@CrossOrigin(origins = "http://localhost:4200")
public interface BulletRepository extends JpaRepository<Bullet, Integer> {

    @Query("select bull from Bullet bull where inches = bull.caliber")
    List<Bullet> getBulletByInches(double inches);

    @Query("select bull from Bullet bull where fromValue <= bull.caliber and toValue >= bull.caliber order by bull.caliber")
    List<Bullet> getBulletByInchesRange(double fromValue, double toValue);

    @Query("select bull from Bullet bull where inches = bull.caliber and bulletType = bull.type")
    List<Bullet> getBulletByInchesAndType(double inches, String bulletType);
}

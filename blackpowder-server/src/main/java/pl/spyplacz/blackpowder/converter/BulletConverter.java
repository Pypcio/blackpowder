package pl.spyplacz.blackpowder.converter;

import pl.spyplacz.blackpowder.dto.BulletDto;
import pl.spyplacz.blackpowder.entity.Bullet;

public class BulletConverter {

    public static BulletDto toDto(Bullet bullet) {
        return new BulletDto(bullet.getId(), bullet.getType(), bullet.getCaliber());
    }

    public static Bullet toEntity(BulletDto bulletDto) {
        Bullet bullet = new Bullet(bulletDto.getType(), bulletDto.getCaliber());
        bullet.setId(bulletDto.getId());
        return bullet;
    }
}

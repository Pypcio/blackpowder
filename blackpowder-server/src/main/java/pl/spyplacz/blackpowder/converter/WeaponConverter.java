package pl.spyplacz.blackpowder.converter;

import pl.spyplacz.blackpowder.dto.WeaponDto;
import pl.spyplacz.blackpowder.entity.Weapon;

public class WeaponConverter {

    public static WeaponDto toDto(Weapon weapon) {
        return new WeaponDto(weapon.getId(), weapon.getName(), weapon.getWeaponType(),
                weapon.getCaliber(), weapon.getShotsAmount());
    }

    public static Weapon toEntity(WeaponDto weaponDto) {
        Weapon weapon = new Weapon(weaponDto.getName(), weaponDto.getType(), weaponDto.getCaliber(), weaponDto.getShotAmount());
        weapon.setId(weaponDto.getId());
        return weapon;
    }
}

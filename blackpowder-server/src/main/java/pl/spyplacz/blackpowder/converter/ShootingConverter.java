package pl.spyplacz.blackpowder.converter;

import pl.spyplacz.blackpowder.dto.ShootingDto;
import pl.spyplacz.blackpowder.entity.Shooting;

public class ShootingConverter {

    public static ShootingDto toDto(Shooting shooting){
        ShootingDto shootingDto = new ShootingDto(
                shooting.getId(),
                WeaponConverter.toDto(shooting.getWeapon()),
                BulletConverter.toDto(shooting.getBulletType()),
                shooting.getPowderGrains(),
                shooting.getDistance(),
                shooting.getZeroResult(),
                shooting.getOneResult(),
                shooting.getTwoResult(),
                shooting.getThreeResult(),
                shooting.getFourResult(),
                shooting.getFiveResult(),
                shooting.getSixResult(),
                shooting.getSevenResult(),
                shooting.getEightResult(),
                shooting.getNineResult(),
                shooting.getTenResult(),
                shooting.getDate()
        );
        return shootingDto;
    }

    public static Shooting toEntity(ShootingDto shootingDto){
        Shooting shooting = new Shooting(
                WeaponConverter.toEntity(shootingDto.getWeapon()),
                BulletConverter.toEntity(shootingDto.getBullet()),
                shootingDto.getPowderGrains(),
                shootingDto.getDistance(),
                shootingDto.getZeroResult(),
                shootingDto.getOneResult(),
                shootingDto.getTwoResult(),
                shootingDto.getThreeResult(),
                shootingDto.getFourResult(),
                shootingDto.getFiveResult(),
                shootingDto.getSixResult(),
                shootingDto.getSevenResult(),
                shootingDto.getEightResult(),
                shootingDto.getNineResult(),
                shootingDto.getTenResult(),
                shootingDto.getDate()
        );
        return shooting;
    }
}

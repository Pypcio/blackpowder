package pl.spyplacz.blackpowder.dto;

import java.sql.Date;

public class ShootingDto {

    private int id;

    private WeaponDto weapon;

    private BulletDto bullet;

    private int powderGrains;

    private int distance;

    private int zeroResult;

    private int oneResult;

    private int twoResult;

    private int threeResult;

    private int fourResult;

    private int fiveResult;

    private int sixResult;

    private int sevenResult;

    private int eightResult;

    private int nineResult;

    private int tenResult;

    private Date date;

    private int sum;

    private double mean;

    private int shotAmount;

    public ShootingDto(int id, WeaponDto weapon, BulletDto bullet, int powderGrains, int distance, int zeroResult, int oneResult, int twoResult, int threeResult, int fourResult, int fiveResult, int sixResult, int sevenResult, int eightResult, int nineResult, int tenResult, Date date) {
        this.id = id;
        this.weapon = weapon;
        this.bullet = bullet;
        this.powderGrains = powderGrains;
        this.distance = distance;
        this.zeroResult = zeroResult;
        this.oneResult = oneResult;
        this.twoResult = twoResult;
        this.threeResult = threeResult;
        this.fourResult = fourResult;
        this.fiveResult = fiveResult;
        this.sixResult = sixResult;
        this.sevenResult = sevenResult;
        this.eightResult = eightResult;
        this.nineResult = nineResult;
        this.tenResult = tenResult;
        this.date = date;
        calculateStatistics();
    }


    public ShootingDto() {
    }

    public int getId() {
        return id;
    }

    public WeaponDto getWeapon() {
        return weapon;
    }

    public void setWeapon(WeaponDto weapon) {
        this.weapon = weapon;
    }

    public BulletDto getBullet() {
        return bullet;
    }

    public void setBullet(BulletDto bullet) {
        this.bullet = bullet;
    }

    public Integer getPowderGrains() {
        return powderGrains;
    }

    public void setPowderGrains(Integer powderGrains) {
        this.powderGrains = powderGrains;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Integer getZeroResult() {
        return zeroResult;
    }

    public void setZeroResult(Integer zeroResult) {
        this.zeroResult = zeroResult;
    }

    public Integer getOneResult() {
        return oneResult;
    }

    public void setOneResult(Integer oneResult) {
        this.oneResult = oneResult;
    }

    public Integer getTwoResult() {
        return twoResult;
    }

    public void setTwoResult(Integer twoResult) {
        this.twoResult = twoResult;
    }

    public Integer getThreeResult() {
        return threeResult;
    }

    public void setThreeResult(Integer threeResult) {
        this.threeResult = threeResult;
    }

    public Integer getFourResult() {
        return fourResult;
    }

    public void setFourResult(Integer fourResult) {
        this.fourResult = fourResult;
    }

    public Integer getFiveResult() {
        return fiveResult;
    }

    public void setFiveResult(Integer fiveResult) {
        this.fiveResult = fiveResult;
    }

    public Integer getSixResult() {
        return sixResult;
    }

    public void setSixResult(Integer sixResult) {
        this.sixResult = sixResult;
    }

    public Integer getSevenResult() {
        return sevenResult;
    }

    public void setSevenResult(Integer sevenResult) {
        this.sevenResult = sevenResult;
    }

    public Integer getEightResult() {
        return eightResult;
    }

    public void setEightResult(Integer eightResult) {
        this.eightResult = eightResult;
    }

    public Integer getNineResult() {
        return nineResult;
    }

    public void setNineResult(Integer nineResult) {
        this.nineResult = nineResult;
    }

    public Integer getTenResult() {
        return tenResult;
    }

    public void setTenResult(Integer tenResult) {
        this.tenResult = tenResult;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public void calculateStatistics() {
        sum = calculateSum();
        shotAmount = calculateShotAmount();
        mean = calculateMean();
    }


    public Integer getSum() {
        return sum;
    }

    public Double getMean() {
        return mean;
    }

    public Integer getShotAmount() {
        return shotAmount;
    }

    private Integer calculateSum() {
        return zeroResult + oneResult + twoResult * 2 + threeResult * 3 + fourResult * 4 + fiveResult * 5 +
                sixResult * 6 + sevenResult * 7 + eightResult * 8 + nineResult * 9 + tenResult * 10;
    }

    private Integer calculateShotAmount() {
        return zeroResult + oneResult + twoResult + threeResult + fourResult + fiveResult +
                sixResult + sevenResult + eightResult + nineResult + tenResult;
    }

    private Double calculateMean() {
        return Double.valueOf((float) sum / (float) shotAmount);
    }
}

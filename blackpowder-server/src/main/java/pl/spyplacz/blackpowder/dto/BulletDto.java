package pl.spyplacz.blackpowder.dto;

public class BulletDto {

    private int id;
    private String type;
    private double caliber;

    public BulletDto(int id, String type, double caliber) {
        this.type = type;
        this.caliber = caliber;
        this.id = id;
    }

    public BulletDto() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getCaliber() {
        return caliber;
    }

    public void setCaliber(double caliber) {
        this.caliber = caliber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

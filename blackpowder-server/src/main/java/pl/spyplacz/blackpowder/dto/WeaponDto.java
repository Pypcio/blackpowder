package pl.spyplacz.blackpowder.dto;

public class WeaponDto {

    private int id;
    private String name;
    private String type;
    private double caliber;
    private int shotAmount;

    public WeaponDto(int id, String name, String type, double caliber, int shotAmount) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.caliber = caliber;
        this.shotAmount = shotAmount;
    }

    public WeaponDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public double getCaliber() {
        return caliber;
    }

    public int getShotAmount() {
        return shotAmount;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setCaliber(double caliber) {
        this.caliber = caliber;
    }

    public void setShotAmount(int shotAmount) {
        this.shotAmount = shotAmount;
    }
}

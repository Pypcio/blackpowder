package pl.spyplacz.blackpowder.controller;

import pl.spyplacz.blackpowder.dto.ShootingDto;
import pl.spyplacz.blackpowder.repository.BulletRepository;
import pl.spyplacz.blackpowder.repository.WeaponRepository;
import pl.spyplacz.blackpowder.service.api.IShootingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static pl.spyplacz.blackpowder.utils.Constants.*;

@RequestMapping(SHOOTING_SERVICE)
@RestController
@CrossOrigin(origins = CROSS_ADDRESS)
public class ShootingController {

    private static final String BEST = "/best";

    private static final String LAST = "/last";

    @Autowired
    IShootingService shootingService;

    @Autowired
    WeaponRepository weaponRepository;

    @Autowired
    BulletRepository bulletRepository;

    @RequestMapping(GET_ALL)
    public List<ShootingDto> getAllShooting() {
        return shootingService.getAllShooting();
    }

    @RequestMapping(GET_BY_ID)
    public Optional<ShootingDto> getShootingById(@PathVariable int id) {
        return shootingService.getById(id);
    }

    @RequestMapping(BEST)
    public Optional<ShootingDto> getBestShooting() {
        return shootingService.getBest();
    }

    @RequestMapping(LAST)
    public Optional<ShootingDto> getLastShooting() {
        return shootingService.getLast();
    }

    @RequestMapping(value = SAVE, method = RequestMethod.POST)
    public void saveShooting(@RequestBody ShootingDto shootingDto) {
        shootingService.saveShooting(shootingDto);
    }

    @RequestMapping(value = UPDATE_BY_ID, method = RequestMethod.PUT)
    public void editShooting(@PathVariable int id, @RequestBody ShootingDto shootingDto){
        shootingService.updateShooting(id, shootingDto);
    }

    @RequestMapping(value = REMOVE_BY_ID, method = RequestMethod.DELETE)
    public void removeShooting(@PathVariable int id){
        Optional<ShootingDto> shooting = shootingService.getById(id);
        if (shooting.isPresent()){
            shootingService.removeById(id);
        }
    }
}

package pl.spyplacz.blackpowder.controller;

import pl.spyplacz.blackpowder.dto.WeaponDto;
import pl.spyplacz.blackpowder.service.api.IWeaponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static pl.spyplacz.blackpowder.utils.Constants.*;

@RequestMapping(WEAPON_SERVICE)
@RestController
@CrossOrigin(origins = CROSS_ADDRESS)
public class WeaponController {

    @Autowired
    IWeaponService weaponService;

    @RequestMapping(GET_ALL)
    public List<WeaponDto> getAllWeapons() {
        return weaponService.getAllWeapons();
    }

    @RequestMapping(GET_BY_INCHES_RANGE)
    public List<WeaponDto> getWeaponsByCaliber(@PathVariable double inchesFrom, @PathVariable double inchesTo) {
        return weaponService.getWeaponsByCaliber(inchesFrom, inchesTo);
    }

    @RequestMapping(GET_BY_ID)
    public Optional<WeaponDto> getWeaponsById(@PathVariable int id) {
        return weaponService.getWeaponById(id);
    }

    @RequestMapping(value = SAVE, method = RequestMethod.POST)
    public void saveWeapon(@RequestBody WeaponDto weaponDto) {
        weaponService.saveWeapon(weaponDto);
    }

    @RequestMapping(value = UPDATE_BY_ID, method = RequestMethod.PUT)
    public void updateWeapon(@PathVariable int id, @RequestBody WeaponDto weaponDto) {
        weaponService.updateWeapon(id, weaponDto);
    }

    @RequestMapping(value = REMOVE_BY_ID, method = RequestMethod.DELETE)
    public void removeWeapon(@PathVariable int id) {
        weaponService.removeWeapon(id);
    }
}

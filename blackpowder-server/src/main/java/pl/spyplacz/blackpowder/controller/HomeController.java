package pl.spyplacz.blackpowder.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import static pl.spyplacz.blackpowder.utils.Constants.CROSS_ADDRESS;

@Controller
@CrossOrigin(origins = CROSS_ADDRESS)
public class HomeController {
    @RequestMapping("/home")
    public String home() {
        return "index";
    }

//    @RequestMapping(ERROR_PATH)
//    public String error(){
//        return "error handling";
//    }
//
//    @Override
//    public String getErrorPath() {
//        return ERROR_PATH;
//    }
}

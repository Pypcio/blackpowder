package pl.spyplacz.blackpowder.controller;

import pl.spyplacz.blackpowder.dto.BulletDto;
import pl.spyplacz.blackpowder.service.api.IBulletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static pl.spyplacz.blackpowder.utils.Constants.*;

@RequestMapping(BULLET_SERVICE)
@RestController
@CrossOrigin(origins = CROSS_ADDRESS)
public class BulletController {

    @Autowired
    IBulletService bulletService;

    @RequestMapping(GET_ALL)
    public List<BulletDto> getAllBullets() {
        return bulletService.getAllBullets();
    }

    @RequestMapping(GET_BY_INCHES_RANGE)
    public List<BulletDto> getBulletsByCaliber(@PathVariable double inchesFrom, @PathVariable double inchesTo) {
        return bulletService.getBulletByInchesRange(inchesFrom, inchesTo);
    }

    @RequestMapping(GET_BY_ID)
    public Optional<BulletDto> getWeaponsById(@PathVariable int id) {
        return bulletService.getBulletById(id);
    }

    @RequestMapping(value = SAVE, method = RequestMethod.POST)
    public void saveBullet(@RequestBody BulletDto bullet) {
        bulletService.save(bullet);
    }

    @RequestMapping(value = UPDATE_BY_ID, method = RequestMethod.PUT)
    public void updateBullet(@PathVariable int id, @RequestBody BulletDto bulletDto) {
        bulletService.updateBullet(id, bulletDto);
    }

    @RequestMapping(value = REMOVE_BY_ID, method = RequestMethod.DELETE)
    public void removeBullet(@PathVariable int id) {
        bulletService.removeBullet(id);
    }

}

package pl.spyplacz.blackpowder.utils;

public class Constants {

    public static final String CROSS_ADDRESS = "http://localhost:4200";

    public static final String ERROR_PATH = "/error";

    public static final String WEAPON_SERVICE = "/weapon";

    public static final String SHOOTING_SERVICE = "/shooting";

    public static final String BULLET_SERVICE = "/bullet";

    public static final String WAD_SERVICE = "/wad";

    public static final String GET_ALL = "getAll";

    public static final String GET_BY_ID = "/getById/{id}";

    public static final String UPDATE_BY_ID = "/update/{id}";

    public static final String REMOVE_BY_ID = "/delete/{id}";

    public static final String GET_BY_INCHES = "/{inches}";

    public static final String ORDER_BY_INCHES = "/sort";

    public static final String GET_BY_INCHES_RANGE = "/{inchesFrom}-{inchesTo}";

    public static final String SAVE = "/save";

}

package pl.spyplacz.blackpowder.entity;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "shooting")
public class Shooting {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "weapon", nullable=false)
    private Weapon weapon;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "bullet", nullable=false)
    private Bullet bulletType;

    @Column(name = "powder_grains")
    private Integer powderGrains;

    @Column(name = "distance")
    private Integer distance;

    @Column(name = "zero_result")
    private Integer zeroResult;

    @Column(name = "one_result")
    private Integer oneResult;

    @Column(name = "two_result")
    private Integer twoResult;

    @Column(name = "three_result")
    private Integer threeResult;

    @Column(name = "four_result")
    private Integer fourResult;

    @Column(name = "five_result")
    private Integer fiveResult;

    @Column(name = "six_result")
    private Integer sixResult;

    @Column(name = "seven_result")
    private Integer sevenResult;

    @Column(name = "eight_result")
    private Integer eightResult;

    @Column(name = "nine_result")
    private Integer nineResult;

    @Column(name = "ten_result")
    private Integer tenResult;

    @Column(name = "date")
    private Date date;

    public Shooting(Weapon weapon, Bullet bulletType, Integer powderGrains, Integer distance, Integer zeroResult,
                    Integer oneResult, Integer twoResult, Integer threeResult, Integer fourResult, Integer fiveResult,
                    Integer sixResult, Integer sevenResult, Integer eightResult, Integer nineResult, Integer tenResult,
                    Date date) {
        this.weapon = weapon;
        this.bulletType = bulletType;
        this.powderGrains = powderGrains;
        this.distance = distance;
        this.zeroResult = zeroResult;
        this.oneResult = oneResult;
        this.twoResult = twoResult;
        this.threeResult = threeResult;
        this.fourResult = fourResult;
        this.fiveResult = fiveResult;
        this.sixResult = sixResult;
        this.sevenResult = sevenResult;
        this.eightResult = eightResult;
        this.nineResult = nineResult;
        this.tenResult = tenResult;
        this.date = date;
    }


    public Shooting() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public Bullet getBulletType() {
        return bulletType;
    }

    public Integer getPowderGrains() {
        return powderGrains;
    }

    public Integer getDistance() {
        return distance;
    }

    public Integer getZeroResult() {
        return zeroResult;
    }

    public Integer getOneResult() {
        return oneResult;
    }

    public Integer getTwoResult() {
        return twoResult;
    }

    public Integer getThreeResult() {
        return threeResult;
    }

    public Integer getFourResult() {
        return fourResult;
    }

    public Integer getFiveResult() {
        return fiveResult;
    }

    public Integer getSixResult() {
        return sixResult;
    }

    public Integer getSevenResult() {
        return sevenResult;
    }

    public Integer getEightResult() {
        return eightResult;
    }

    public Integer getNineResult() {
        return nineResult;
    }

    public Integer getTenResult() {
        return tenResult;
    }

    public Date getDate() {
        return date;
    }

}

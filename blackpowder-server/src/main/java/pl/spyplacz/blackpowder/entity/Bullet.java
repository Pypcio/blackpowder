package pl.spyplacz.blackpowder.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "bullets")
public class Bullet implements Serializable {

    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "bullet_type")
    private String type;

    @Column(name = "caliber")
    private Double caliber;

    public Bullet(String type, Double caliber) {
        this.type = type;
        this.caliber = caliber;
    }

    public Bullet() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getCaliber() {
        return caliber;
    }

    public void setCaliber(Double caliber) {
        this.caliber = caliber;
    }
}

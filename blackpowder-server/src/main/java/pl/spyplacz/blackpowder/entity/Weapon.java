package pl.spyplacz.blackpowder.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "weapons")
public class Weapon implements Serializable {

    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "weapon_type")
    private String weaponType;

    @Column(name = "caliber")
    private Double caliber;

    @Column(name = "shots_amount")
    private Integer shotsAmount;

    public Weapon(String name, String weaponType, Double caliber, Integer shotsAmount) {
        this.name = name;
        this.weaponType = weaponType;
        this.caliber = caliber;
        this.shotsAmount = shotsAmount;
    }

    public Weapon() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(String weaponType) {
        this.weaponType = weaponType;
    }

    public double getCaliber() {
        return caliber;
    }

    public void setCaliber(double caliber) {
        this.caliber = caliber;
    }

    public int getShotsAmount() {
        return shotsAmount;
    }

    public void setShotsAmount(int shotsAmount) {
        this.shotsAmount = shotsAmount;
    }
}

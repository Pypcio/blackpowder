import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {WeaponListComponent} from "./weapon-list/weapon-list.component";
import {ShootingListComponent} from "./shooting-list/shooting-list.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DemoMaterialModule} from "./material-module";
import {WeaponAddComponent} from './weapon-add/weapon-add.component';
import {WeaponEditComponent} from './weapon-edit/weapon-edit.component';
import {AppRoutingModule} from "./app-routing.module";
import {OktaAuthModule} from "@okta/okta-angular";
import {WeaponService} from "./shared/weapon/weapon.service";
import {AuthInterceptor} from "./shared/okta/auth.interceptor";
import { HomeComponent } from './home/home.component';
import { BulletListComponent } from './bullet-list/bullet-list.component';
import { BulletAddComponent } from './bullet-add/bullet-add.component';
import { BulletEditComponent } from './bullet-edit/bullet-edit.component';
import { ShootingAddComponent } from './shooting-add/shooting-add.component';
import { ShootingEditComponent } from './shooting-edit/shooting-edit.component';

const config = {
  issuer: 'https://dev-997399.okta.com/oauth2/default',
  redirectUri: 'http://localhost:4200/implicit/callback',
  clientId: '0oae801jlu0ngNLSC356'
};

// @ts-ignore
@NgModule({
  declarations: [
    AppComponent,
    ShootingListComponent,
    WeaponListComponent,
    WeaponAddComponent,
    WeaponEditComponent,
    HomeComponent,
    BulletListComponent,
    BulletAddComponent,
    BulletEditComponent,
    ShootingAddComponent,
    ShootingEditComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    OktaAuthModule.initAuth(config)
  ],
  providers: [WeaponService,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule {

}

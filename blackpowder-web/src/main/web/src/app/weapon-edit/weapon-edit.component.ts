import { Component, OnInit } from '@angular/core';
import {WeaponService} from "../shared/weapon/weapon.service";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormControl, FormGroup, NgForm} from "@angular/forms";

@Component({
  selector: 'app-weapon-edit',
  templateUrl: './weapon-edit.component.html',
  styleUrls: ['./weapon-edit.component.css']
})
export class WeaponEditComponent implements OnInit {

  weaponForm: FormGroup;
  private id: number;
  sub: Subscription;


  constructor(private route: ActivatedRoute,
              private router: Router,
              private weaponService: WeaponService) {

    this.weaponForm = new FormGroup({
      name: new FormControl(),
      caliber: new FormControl(),
      type: new FormControl(),
      shotAmount: new FormControl()
    })

  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      if (this.id) {
        this.weaponService.get(this.id).subscribe((weapon: any) => {
          if (weapon) {
            this.weaponForm.setValue({
              name: weapon.name,
              shotAmount: weapon.shotAmount,
              type: weapon.type,
              caliber: weapon.caliber
            });
          } else {
            this.goToWeaponList();
          }
        });
      }
    });
  }

  save() {
    this.weaponService.update(this.id, this.weaponForm.value).subscribe(result => {
      this.goToWeaponList();
    }, error1 => console.error(error1));
  }

  goToWeaponList() {
    this.router.navigate(['/weapon-list'])
  }

  remove (){
    this.weaponService.remove(String(this.id)).subscribe(result => {
      this.goToWeaponList();
    }, error => console.error(error));
  }

}


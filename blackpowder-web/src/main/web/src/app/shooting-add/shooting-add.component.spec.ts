import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShootingAddComponent } from './shooting-add.component';

describe('ShootingAddComponent', () => {
  let component: ShootingAddComponent;
  let fixture: ComponentFixture<ShootingAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShootingAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShootingAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

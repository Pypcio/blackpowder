import {Component, OnInit} from '@angular/core';
import {ShootingService} from "../shared/shooting/shooting.service";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BulletService} from "../shared/bullet/bullet.service";
import {WeaponService} from "../shared/weapon/weapon.service";

@Component({
  selector: 'app-shooting-add',
  templateUrl: './shooting-add.component.html',
  styleUrls: ['./shooting-add.component.css']
})
export class ShootingAddComponent implements OnInit {

  shootingForm: FormGroup;
  bullets: Array<any>;
  weapons: Array<any>;
  type: String;


  constructor(private bulletService: BulletService,
              private weaponService: WeaponService,
              private shootingService: ShootingService,
              private router: Router,
              private fb: FormBuilder) {

    this.shootingForm = this.fb.group({
        "weapon": [null, Validators.required],
        "bullet": [null, Validators.required],
        "distance": [null, Validators.required],
        "powderGrains": [null, Validators.required],
        "date": [null, Validators.required],
        "zeroResult": [null, Validators.required],
        "oneResult": [null, Validators.required],
        "twoResult": [null, Validators.required],
        "threeResult": [null, Validators.required],
        "fourResult": [null, Validators.required],
        "fiveResult": [null, Validators.required],
        "sixResult": [null, Validators.required],
        "sevenResult": [null, Validators.required],
        "eightResult": [null, Validators.required],
        "nineResult": [null, Validators.required],
        "tenResult": [null, Validators.required]
      }
    );

    this.shootingForm.setValue({
      weapon: "",
      bullet: "",
      distance: "",
      powderGrains: "",
      date: "",
      zeroResult: 0,
      oneResult: 0,
      twoResult: 0,
      threeResult: 0,
      fourResult: 0,
      fiveResult: 0,
      sixResult: 0,
      sevenResult: 0,
      eightResult: 0,
      nineResult: 0,
      tenResult: 0
    })

  }

  ngOnInit() {
    this.bulletService.getAll().subscribe(data => {
      this.bullets = data;
      this.weaponService.getAll().subscribe(data => {
        this.weapons = data;
      })
    });
  }

  goToShootingList() {
    this.router.navigate(['/shooting-list']);
  }

  save() {
    console.log(this.shootingForm);
    this.weaponService.get(this.shootingForm.value.weapon).subscribe(data => {
      this.shootingForm.value.weapon = data;
      this.bulletService.get(this.shootingForm.value.bullet).subscribe(data => {
        this.shootingForm.value.bullet = data;
        console.log(this.shootingForm);
        this.shootingService.save(this.shootingForm.value).subscribe(() => {
          this.goToShootingList();
        });
      });
    });
  }
}

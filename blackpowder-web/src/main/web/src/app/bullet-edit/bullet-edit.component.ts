import {Component, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {BulletService} from "../shared/bullet/bullet.service";
import {FormBuilder, FormGroup, Validators, FormControl} from "@angular/forms";

@Component({
  selector: 'app-bullet-edit',
  templateUrl: './bullet-edit.component.html',
  styleUrls: ['./bullet-edit.component.css']
})
export class BulletEditComponent implements OnInit {

  private id: number;
  bulletForm: FormGroup;
  sub: Subscription;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private bulletService: BulletService) {

    this.bulletForm = new FormGroup({
      type: new FormControl(),
      caliber: new FormControl()
    });
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      if (this.id) {
        this.bulletService.get(this.id).subscribe((bullet: any) => {
          if (bullet) {
            this.bulletForm.setValue({
              type: bullet.type,
              caliber: bullet.caliber
            });
            this.id = bullet.id;
          } else {
            this.goToBulletList();
          }
        });
      }
    });
  }

  save() {
    console.log(this.bulletForm);
    this.bulletService.update(this.id, this.bulletForm.value).subscribe(result => {
      this.goToBulletList();
    }, error => console.error(error));
  }

  remove() {
    this.bulletService.remove(String(this.id)).subscribe(result => {
      this.goToBulletList();
    }, error => console.error(error));
  }

  goToBulletList() {
    this.router.navigate(['/bullet-list'])
  }


}

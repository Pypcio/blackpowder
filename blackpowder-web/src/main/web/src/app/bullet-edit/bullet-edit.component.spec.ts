import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulletEditComponent } from './bullet-edit.component';

describe('BulletEditComponent', () => {
  let component: BulletEditComponent;
  let fixture: ComponentFixture<BulletEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulletEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulletEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

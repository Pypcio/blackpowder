import {Component, OnInit} from '@angular/core';
import {BulletService} from "../shared/bullet/bullet.service";

@Component({
  selector: 'app-bullet-list',
  templateUrl: './bullet-list.component.html',
  styleUrls: ['./bullet-list.component.css']
})
export class BulletListComponent implements OnInit {
  bullets: Array<any>;

  constructor(private bulletService: BulletService) {

  }

  ngOnInit() {
    this.bulletService.getAll().subscribe(data => {
      this.bullets = data;
    })
  }

}

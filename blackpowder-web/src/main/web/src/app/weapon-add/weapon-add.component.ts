import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";
import {WeaponService} from "../shared/weapon/weapon.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-weapon',
  templateUrl: './weapon-add.component.html',
  styleUrls: ['./weapon-add.component.css']
})
export class WeaponAddComponent implements OnInit {

  weaponForm: FormGroup;
  caliber: Number;
  shotAmount: Number;



  constructor(private weaponService: WeaponService,
              private router: Router,
              private fb: FormBuilder) {

    this.weaponForm = this.fb.group({
        "name": [null, Validators.required],
        "caliber": [null, Validators.required],
        "shotAmount": [null, Validators.required],
        "type": [null, Validators.required],
      }
    );

  }

  ngOnInit() {
  }

  goToWeaponList(){
    this.router.navigate(['/weapon-list']);
  }

  save() {
    console.log(this.weaponForm);
    this.weaponService.save(this.weaponForm.value).subscribe(result => {
      this.goToWeaponList();
    }, error1 => console.error(error1));
  }


}

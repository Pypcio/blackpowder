import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WeaponListComponent} from "./weapon-list/weapon-list.component";
import {WeaponAddComponent} from "./weapon-add/weapon-add.component";
import {WeaponEditComponent} from "./weapon-edit/weapon-edit.component";
import {OktaCallbackComponent} from "@okta/okta-angular";
import {HomeComponent} from "./home/home.component";
import {ShootingListComponent} from "./shooting-list/shooting-list.component";
import {BulletListComponent} from "./bullet-list/bullet-list.component";
import {BulletAddComponent} from "./bullet-add/bullet-add.component";
import {BulletEditComponent} from "./bullet-edit/bullet-edit.component";
import {ShootingAddComponent} from "./shooting-add/shooting-add.component";
import {ShootingEditComponent} from "./shooting-edit/shooting-edit.component";

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {
    path: 'weapon-list',
    component: WeaponListComponent
  },
  {
    path: 'weapon-add',
    component: WeaponAddComponent
  },
  {
    path: 'weapon-edit/:id',
    component: WeaponEditComponent
  },
  {
    path: 'implicit/callback',
    component: OktaCallbackComponent
  },
  {
    path: 'shooting-list',
    component: ShootingListComponent
  },
  {
    path: 'bullet-list',
    component: BulletListComponent
  },
  {
    path: 'bullet-add',
    component: BulletAddComponent
  },
  {
    path: 'bullet-edit/:id',
    component: BulletEditComponent
  },
  {
    path: 'shooting-add',
    component: ShootingAddComponent
  },
  {
    path: 'shooting-edit/:id',
    component: ShootingEditComponent
  },
  {
    path: 'implicit/callback',
    component: OktaCallbackComponent
  },
  {
    path: '',
    component: HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

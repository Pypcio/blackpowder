import {Component, OnInit} from '@angular/core';
import {WeaponService} from "../shared/weapon/weapon.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-weapon-list',
  templateUrl: './weapon-list.component.html',
  styleUrls: ['./weapon-list.component.css']
})

export class WeaponListComponent implements OnInit {
  weapons: Array<any>;
  options: FormGroup;

  constructor(private weaponService: WeaponService, fb: FormBuilder, private router: Router, private route: ActivatedRoute) {
    this.options = fb.group({
      hideRequired: false,
      floatLabel: 'auto',
    });
  }

  ngOnInit() {
    this.weaponService.getAll().subscribe(data => {
      this.weapons = data;
    })
  }

}

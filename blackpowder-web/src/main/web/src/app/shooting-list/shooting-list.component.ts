import { Component, OnInit } from '@angular/core';
import {ShootingService} from "../shared/shooting/shooting.service";

@Component({
  selector: 'app-shooting-list',
  templateUrl: './shooting-list.component.html',
  styleUrls: ['./shooting-list.component.css']
})
export class ShootingListComponent implements OnInit {
  shootings: Array<any>;

  constructor(private shootingService : ShootingService) { }

  ngOnInit() {
    this.shootingService.getAll().subscribe(data => {
      this.shootings = data;
    })
  }

}

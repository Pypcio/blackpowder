import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShootingListComponent } from './shooting-list.component';

describe('ShootingListComponent', () => {
  let component: ShootingListComponent;
  let fixture: ComponentFixture<ShootingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShootingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShootingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

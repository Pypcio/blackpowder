import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ShootingService {

  private API = '//localhost:8080'
  private SHOOTING_API = this.API + '/shooting';

  constructor(private http: HttpClient) {

  }

  getAll(): Observable<any>{
    return this.http.get(this.SHOOTING_API+'/getAll')
  }

  getBest(): Observable<any>{
    return this.http.get(this.SHOOTING_API+'/best')
  }

  getLast(): Observable<any>{
    return this.http.get(this.SHOOTING_API+'/last')
  }

  get(id: number): Observable<any>{
    return this.http.get(this.SHOOTING_API+'/getById/'+id);
  }

  save(shooting: any): Observable<any>{
    console.log(shooting);
    let result: Observable<Object>;
    if(shooting.id != null){
      result = this.http.put(shooting.id, shooting);
    } else {
      result = this.http.post(this.SHOOTING_API+'/save', shooting)
    }
    return result;
  }

  update(id:number , shooting: any): Observable<any>{
    let result: Observable<Object>;
    result = this.http.put(this.SHOOTING_API+'/update/' + id, shooting);
    return result;
  }

  remove(href: string){
    console.log(this.SHOOTING_API+'/delete/'+href);
    return this.http.delete(this.SHOOTING_API+'/delete/'+href);
  }

}



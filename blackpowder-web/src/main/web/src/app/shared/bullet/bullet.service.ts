import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class BulletService {


  private API = '//localhost:8080'
  private BULLET_API = this.API + '/bullet';


  constructor(private http: HttpClient) {

  }

  getAll(): Observable<any>{
    return this.http.get(this.BULLET_API+'/getAll')
  }

  getByInches(inches): Observable<any>{
    return this.http.get(this.BULLET_API+'/inches/'+inches)
  }

  get(id): Observable<any>{
    return this.http.get(this.BULLET_API+'/getById/'+id)
  }

  save(bullet: any): Observable<any>{
    let result: Observable<Object>;
    if(bullet.id != null){
      result = this.http.put(bullet.id, bullet);
    } else {
      result = this.http.post(this.BULLET_API+'/save', bullet)
    }
    return result;
  }

  update(id:number , bullet: any): Observable<any>{
    let result: Observable<Object>;
    result = this.http.put(this.BULLET_API+'/update/' + id, bullet);
    return result;
  }

  remove(href: string){
    return this.http.delete(this.BULLET_API+"/delete/" + href);
  }

}


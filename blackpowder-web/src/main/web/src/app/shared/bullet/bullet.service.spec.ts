import { TestBed } from '@angular/core/testing';

import { BulletService } from './bullet.service';

describe('BulletService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BulletService = TestBed.get(BulletService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeaponService {
  private API = '//localhost:8080';
  private WEAPON_API = this.API + '/weapon';
  private GET_BY_ID = '/getById';

  constructor(private http: HttpClient) {

  }

  getAll(): Observable<any>{
    return this.http.get(this.WEAPON_API + '/getAll');
  }

  get(id: number){
    return this.http.get(this.WEAPON_API + this.GET_BY_ID+ '/' + id);
  }

  save(weapon: any): Observable<any>{
    let result: Observable<Object>;
    if(weapon.id != null){
      result = this.http.put(weapon.id, weapon);
    } else {
      result = this.http.post(this.WEAPON_API+'/save', weapon)
    }
    return result;
  }

  update(id:number , weapon: any): Observable<any>{
    let result: Observable<Object>;
      result = this.http.put(this.WEAPON_API+'/update/' + id, weapon);
    return result;
  }

  remove(href: string){
    return this.http.delete(this.WEAPON_API+'/delete/'+href);
  }

}

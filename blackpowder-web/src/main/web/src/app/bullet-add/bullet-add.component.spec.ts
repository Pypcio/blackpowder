import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulletAddComponent } from './bullet-add.component';

describe('BulletAddComponent', () => {
  let component: BulletAddComponent;
  let fixture: ComponentFixture<BulletAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulletAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulletAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

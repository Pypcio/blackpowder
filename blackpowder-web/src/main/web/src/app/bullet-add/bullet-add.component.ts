import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";
import {BulletService} from "../shared/bullet/bullet.service";

@Component({
  selector: 'app-bullet-add',
  templateUrl: './bullet-add.component.html',
  styleUrls: ['./bullet-add.component.css']
})
export class BulletAddComponent implements OnInit {
  bulletForm: FormGroup;
  caliber: Number;
  type: String;


  constructor(private bulletService: BulletService,
              private router: Router,
              private fb: FormBuilder) {

    this.bulletForm = this.fb.group({
        "caliber": [null, Validators.required],
        "type": [null, Validators.required]
      }
    );

  }

  ngOnInit() {
  }

  goToBulletList() {
    this.router.navigate(['/bullet-list']);
  }

  save() {
    console.log(this.bulletForm);
    this.bulletService.save(this.bulletForm.value).subscribe(result => {
      this.goToBulletList();
    }, error1 => console.error(error1));
  }


}

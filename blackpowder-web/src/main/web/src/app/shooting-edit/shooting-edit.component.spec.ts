import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShootingEditComponent } from './shooting-edit.component';

describe('ShootingEditComponent', () => {
  let component: ShootingEditComponent;
  let fixture: ComponentFixture<ShootingEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShootingEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShootingEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

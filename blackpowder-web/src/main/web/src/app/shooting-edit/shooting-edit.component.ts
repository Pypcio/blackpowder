import {Component, OnInit} from '@angular/core';
import {BulletService} from "../shared/bullet/bullet.service";
import {WeaponService} from "../shared/weapon/weapon.service";
import {ShootingService} from "../shared/shooting/shooting.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-shooting-edit',
  templateUrl: './shooting-edit.component.html',
  styleUrls: ['./shooting-edit.component.css']
})

// this service allow currently only to remove entity because
export class ShootingEditComponent implements OnInit {
  bullets = [];
  weapons = [];
  type: String;

  shootingForm: FormGroup;

  id: number;

  sub: Subscription;

  constructor(private bulletService: BulletService,
              private weaponService: WeaponService,
              private shootingService: ShootingService,
              private router: Router,
              private route: ActivatedRoute) {


    this.shootingForm = new FormGroup({
      weapon: new FormControl(),
      bullet: new FormControl(),
      powderGrains: new FormControl(),
      distance: new FormControl(),
      zeroResult: new FormControl(),
      oneResult: new FormControl(),
      twoResult: new FormControl(),
      threeResult: new FormControl(),
      fourResult: new FormControl(),
      fiveResult: new FormControl(),
      sixResult: new FormControl(),
      sevenResult: new FormControl(),
      eightResult: new FormControl(),
      nineResult: new FormControl(),
      tenResult: new FormControl(),
      date: new FormControl()
    });
  }


  ngOnInit() {
    this.bulletService.getAll().subscribe(data => {
      this.bullets = data;
      this.weaponService.getAll().subscribe(data => {
        this.weapons = data;
        this.sub = this.route.params.subscribe(params => {
          this.id = params['id'];
          if (this.id) {
            this.shootingService.get(this.id).subscribe((shooting: any) => {
              let weaponId = this.calculateWeaponId(shooting.weapon.id);
              let bulletId = this.calculateBulletId(shooting.bullet.id);
              if (shooting) {
                this.shootingForm.setValue({
                  weapon: this.weapons[weaponId].id,
                  bullet: this.bullets[bulletId].id,
                  powderGrains: shooting.powderGrains,
                  distance: shooting.distance,
                  zeroResult: shooting.zeroResult,
                  oneResult: shooting.oneResult,
                  twoResult: shooting.twoResult,
                  threeResult: shooting.threeResult,
                  fourResult: shooting.fourResult,
                  fiveResult: shooting.fiveResult,
                  sixResult: shooting.sixResult,
                  sevenResult: shooting.sevenResult,
                  eightResult: shooting.eightResult,
                  nineResult: shooting.nineResult,
                  tenResult: shooting.tenResult,
                  date: shooting.date
                });
              } else {
                this.goToShootingList();
              }
            });
          }
        });
      });
    });
  }

  save() {
    console.debug(this.shootingForm);
    this.weaponService.get(this.shootingForm.value.weapon).subscribe(data => {
      this.shootingForm.value.weapon = data;
      this.bulletService.get(this.shootingForm.value.bullet).subscribe(data => {
        this.shootingForm.value.bullet = data;
        console.debug("filled form: " + this.shootingForm);
        this.shootingService.update(this.id, this.shootingForm.value).subscribe(result => {
          this.goToShootingList();
        }, error => console.error(error));
      });
    });
  }


  remove() {
    this.shootingService.remove(String(this.id)).subscribe(result => {
      this.goToShootingList();
    }, error => console.error(error));
  }

  goToShootingList() {
    this.router.navigate(['/shooting-list']);
  }

  calculateWeaponId(shootingWeaponId: number): number {
    let weaponId: number;
    console.log(this.weapons);
    for (let i = 0; i < this.weapons.length; i++) {
      weaponId = this.weapons[i].id;
      if (weaponId == shootingWeaponId) {
        console.debug("find weapon: " + weaponId);
        return i;
      }
    }
    console.warn("weapon");
    return;
  }

  calculateBulletId(shootingBulletId: number): number {
    let bulletId: number;
    console.log(this.bullets);
    for (let i = 0; i < this.bullets.length; i++) {
      bulletId = this.bullets[i].id;
      if (bulletId == shootingBulletId) {
        console.debug("find bullet: " + bulletId);
        return i;
      }
    }
    console.warn("bullet");
    return;
  }

}

function delay(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
